-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-02-2021 a las 15:30:29
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `facturacion`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE  PROCEDURE `actualizar_precio_producto` (`n_cantidad` INT, `n_precio` DECIMAL(10,2), `codigo` INT)  BEGIN
    	DECLARE nueva_existencia int;
        DECLARE nuevo_total  decimal(10,2);
        DECLARE nuevo_precio decimal(10,2);
        
        DECLARE cant_actual int;
        DECLARE pre_actual decimal(10,2);
        
        DECLARE actual_existencia int;
        DECLARE actual_precio decimal(10,2);
                
        SELECT precio,existencia INTO actual_precio,actual_existencia FROM producto WHERE codproducto = codigo;
        SET nueva_existencia = actual_existencia + n_cantidad;
        SET nuevo_total = (actual_existencia * actual_precio) + (n_cantidad * n_precio);
        SET nuevo_precio = nuevo_total / nueva_existencia;
        
        UPDATE producto SET existencia = nueva_existencia, precio = nuevo_precio WHERE codproducto = codigo;
        
        SELECT nueva_existencia,nuevo_precio;
        
    END$$

CREATE  PROCEDURE `add_detalle_temp` (`codigo` INT, `cantidad` INT, `token_user` VARCHAR(50))  BEGIN
    	
        DECLARE precio_actual decimal(10,2);
        SELECT precio INTO precio_actual FROM producto WHERE codproducto = codigo;
        
        INSERT INTO detalle_temp (token_user, codproducto, cantidad, precio_venta ) VALUES(token_user, codigo, cantidad, precio_actual );
        SELECT tmp.correlativo, tmp.codproducto, p.descripcion, tmp.cantidad, tmp.precio_venta FROM detalle_temp tmp
        INNER JOIN producto p 
        ON tmp.codproducto = p.codproducto
        WHERE tmp.token_user= token_user;
    END$$

CREATE  PROCEDURE `anular_factura` (`no_factura` INT)  BEGIN
        
        DECLARE existe_factura INT;
        DECLARE registros INT;
        DECLARE a INT;
        
        DECLARE cod_producto INT;
        DECLARE cant_producto INT;
        DECLARE existencia_actual INT;
        DECLARE nueva_existencia INT;
        
        
        SET existe_factura = (SELECT COUNT(*) FROM factura WHERE nofactura = no_factura and estatus=1);
        
        IF existe_factura > 0 THEN
        
            CREATE TEMPORARY TABLE tbl_tmp (
                        id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        cod_prod BIGINT,
                        cant_prod int);
                        
                   		 SET a=1;
                    
                    SET registros = (SELECT COUNT(*) FROM detallefactura WHERE nofactura = no_factura);
                    
                    IF registros > 0 THEN
                    
                        INSERT INTO tbl_tmp(cod_prod,cant_prod) SELECT codproducto, cantidad FROM detallefactura WHERE nofactura = no_factura; 
                       
                        WHILE a <= registros DO
                       
                            SELECT cod_prod, cant_prod INTO cod_producto , cant_producto FROM tbl_tmp WHERE id=a;

                            SELECT existencia INTO existencia_actual FROM producto WHERE codproducto=cod_producto;

                            SET nueva_existencia= existencia_actual + cant_producto;
                            
                            UPDATE producto SET existencia=nueva_existencia WHERE codproducto=cod_producto;
                            SET a=a+1;                        
                        END WHILE;

                        UPDATE factura SET estatus=2 WHERE nofactura=no_factura;                        
                        DROP TABLE tbl_tmp;                        
                        SELECT * FROM factura WHERE nofactura=no_factura;                    
                     END IF;
        ELSE
        	SELECT 0 factura;
        END IF;
      END$$

CREATE  PROCEDURE `DataDashboard` ()  BEGIN
         	DECLARE usuarios INT;
            DECLARE clientes INT;
            DECLARE proveedores INT;
         	DECLARE productos INT;
          	DECLARE ventas INT;

			SELECT COUNT(*) INTO  usuarios FROM usuario WHERE estatus!=10;
            SELECT COUNT(*) INTO  clientes FROM cliente WHERE estatus!=10;
            SELECT COUNT(*) INTO  proveedores FROM proveedor WHERE estatus!=10;
            SELECT COUNT(*) INTO  productos FROM producto WHERE estatus!=10;
            SELECT COUNT(*) INTO  ventas FROM factura WHERE fecha > CURDATE() AND estatus!=10;
            
            SELECT usuarios,clientes,proveedores,productos,ventas;


      END$$

CREATE  PROCEDURE `del_detalle_temp` (`id_detalle` INT, `token` VARCHAR(50))  BEGIN 
    	DELETE FROM detalle_temp WHERE correlativo= id_detalle;
        
        SELECT tmp.correlativo, tmp.codproducto, p.descripcion, tmp.cantidad, tmp.precio_venta FROM detalle_temp tmp
        INNER JOIN producto p on tmp.codproducto=p.codproducto
        WHERE tmp.token_user=token;
     END$$

CREATE  PROCEDURE `procesar_venta` (`cod_usuario` INT, `cod_cliente` INT, `token` VARCHAR(50))  BEGIN
        	DECLARE factura INT;
           
        	DECLARE registros INT;
            DECLARE total DECIMAL(10,2);
            
            DECLARE nueva_existencia int;
            DECLARE existencia_actual int;
            
            DECLARE tmp_cod_producto int;
            DECLARE tmp_cant_producto int;
            DECLARE a INT;
            SET a = 1;
            
            CREATE TEMPORARY TABLE tbl_tmp_tokenuser (
                	id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                	cod_prod BIGINT,
                	cant_prod int);
             SET registros = (SELECT COUNT(*) FROM detalle_temp WHERE token_user = token);
             
             IF registros > 0 THEN 
             	INSERT INTO tbl_tmp_tokenuser(cod_prod,cant_prod) SELECT codproducto,cantidad FROM detalle_temp WHERE token_user = token;
                
                INSERT INTO factura(usuario,codcliente) VALUES(cod_usuario,cod_cliente);
                SET factura = LAST_INSERT_ID();
                
                INSERT INTO detallefactura(nofactura,codproducto,cantidad,precio_venta) SELECT (factura) as nofactura, codproducto,cantidad,precio_venta 				FROM detalle_temp WHERE token_user = token; 
                
                WHILE a <= registros DO
                	SELECT cod_prod,cant_prod INTO tmp_cod_producto,tmp_cant_producto FROM tbl_tmp_tokenuser WHERE id = a;
                    SELECT existencia INTO existencia_actual FROM producto WHERE codproducto = tmp_cod_producto;
                    
                    SET nueva_existencia = existencia_actual - tmp_cant_producto;
                    UPDATE producto SET existencia = nueva_existencia WHERE codproducto = tmp_cod_producto;
                    
                    SET a=a+1;
                    
                
                END WHILE; 
                
                SET total = (SELECT SUM(cantidad * precio_venta) FROM detalle_temp WHERE token_user = token);
                UPDATE factura SET totalfactura = total WHERE nofactura = factura;
                DELETE FROM detalle_temp WHERE token_user = token;
                TRUNCATE TABLE tbl_tmp_tokenuser;
                SELECT * FROM factura WHERE nofactura = factura;
             
             ELSE
             SELECT 0;
             END IF;
            END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `nit` int(11) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `telefono` varchar(11) DEFAULT NULL,
  `direccion` text DEFAULT NULL,
  `dateadd` datetime NOT NULL DEFAULT current_timestamp(),
  `usuario_id` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idcliente`, `nit`, `nombre`, `telefono`, `direccion`, `dateadd`, `usuario_id`, `estatus`) VALUES
(1, 0, 'cf', '0981967284', 'los bajos', '2021-01-16 11:20:48', 1, 1),
(5, 1, 'Luis ', '09764', 'manta', '2021-02-09 23:13:56', 1, 1),
(6, 444, 'jose', '2356', 'pajan', '2021-02-09 23:51:12', 1, 1),
(7, 9, 'harry potter ', '09909909909', 'Howar', '2021-02-10 16:45:15', 1, 1),
(8, 10, 'santy', '042121', 'fgf', '2021-02-13 23:42:49', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id` bigint(20) NOT NULL,
  `nit` varchar(20) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `razon_social` varchar(100) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `direccion` text NOT NULL,
  `iva` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id`, `nit`, `nombre`, `razon_social`, `telefono`, `email`, `direccion`, `iva`) VALUES
(1, '33', 'Mini Market  J.Perez', '', 21545, 'jpminimarket@gmail.com', 'Calle principal, los bajos de afuera, Montecristi, Ecuador ', '12.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallefactura`
--

CREATE TABLE `detallefactura` (
  `correlativo` bigint(11) NOT NULL,
  `nofactura` bigint(11) DEFAULT NULL,
  `codproducto` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_venta` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detallefactura`
--

INSERT INTO `detallefactura` (`correlativo`, `nofactura`, `codproducto`, `cantidad`, `precio_venta`) VALUES
(1, 1, 12, 3, '250.00'),
(2, 1, 3, 1, '20.17'),
(3, 1, 3, 12, '20.17'),
(4, 2, 3, 20, '20.17'),
(5, 3, 3, 1, '20.17'),
(6, 3, 13, 10, '0.45'),
(7, 3, 14, 3, '4.00'),
(8, 4, 3, 1, '20.17'),
(9, 4, 14, 3, '4.00'),
(10, 4, 13, 1, '0.45'),
(11, 5, 13, 1, '0.45'),
(12, 6, 3, 12, '20.17'),
(13, 6, 14, 1, '5.25'),
(15, 7, 5, 18, '21.16'),
(16, 7, 12, 15, '250.00'),
(18, 8, 13, 1, '0.45'),
(19, 8, 15, 16, '0.30'),
(21, 9, 13, 1, '0.45'),
(22, 10, 3, 1, '20.17'),
(23, 11, 3, 5, '20.17'),
(24, 11, 14, 1, '5.25'),
(26, 12, 14, 1, '5.25'),
(27, 12, 15, 4, '0.30'),
(29, 13, 14, 5, '0.50'),
(30, 13, 15, 5, '0.30'),
(31, 13, 16, 8, '0.25'),
(32, 14, 14, 10, '0.50'),
(33, 15, 15, 5, '0.30'),
(34, 16, 3, 10, '20.17'),
(35, 16, 17, 2, '0.75'),
(36, 17, 6, 10, '348.50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_temp`
--

CREATE TABLE `detalle_temp` (
  `correlativo` int(11) NOT NULL,
  `token_user` varchar(50) NOT NULL,
  `codproducto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas`
--

CREATE TABLE `entradas` (
  `correlativo` int(11) NOT NULL,
  `codproducto` int(11) NOT NULL,
  `fecha` datetime NOT NULL DEFAULT current_timestamp(),
  `cantidad` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `usuario_id` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `entradas`
--

INSERT INTO `entradas` (`correlativo`, `codproducto`, `fecha`, `cantidad`, `precio`, `usuario_id`) VALUES
(3, 3, '2021-01-20 22:12:16', 22, '25.00', 1),
(5, 5, '2021-01-21 15:03:38', 50, '1.00', 1),
(6, 6, '2021-01-22 11:56:31', 15, '450.00', 1),
(11, 11, '2021-01-24 19:11:30', 7, '888.00', 1),
(12, 12, '2021-02-01 18:20:46', 200, '250.00', 1),
(13, 11, '2021-02-01 21:43:49', 3, '20.00', 1),
(14, 13, '2021-02-01 22:39:18', 0, '25.00', 1),
(15, 14, '2021-02-01 23:23:36', 0, '25.00', 1),
(16, 3, '2021-02-02 10:23:53', 5, '15.00', 1),
(17, 3, '2021-02-02 10:53:25', 5, '21.00', 1),
(18, 3, '2021-02-02 11:03:46', 3, '22.00', 1),
(19, 3, '2021-02-02 11:23:05', 2, '25.00', 1),
(20, 3, '2021-02-02 11:24:33', 1, '44.00', 1),
(21, 3, '2021-02-02 11:27:27', 2, '44.00', 1),
(22, 6, '2021-02-02 11:38:50', 5, '44.00', 1),
(23, 3, '2021-02-02 11:53:15', 2, '25.00', 1),
(24, 3, '2021-02-02 11:59:57', 2, '44.00', 1),
(25, 5, '2021-02-02 12:00:47', 3, '22.00', 1),
(26, 15, '2021-02-02 12:18:40', 55, '44.00', 1),
(27, 3, '2021-02-02 12:20:01', 3, '44.00', 1),
(28, 5, '2021-02-02 12:21:54', 44, '44.00', 1),
(29, 16, '2021-02-07 08:53:53', 25, '0.25', 1),
(30, 16, '2021-02-07 08:54:12', 23, '0.26', 1),
(31, 17, '2021-02-08 11:37:44', 8, '0.75', 1),
(32, 3, '2021-02-09 21:59:21', 5, '22.00', 1),
(33, 13, '2021-02-11 20:40:29', 15, '0.45', 1),
(34, 14, '2021-02-11 20:40:55', 5, '4.00', 1),
(35, 14, '2021-02-14 13:34:43', 5, '5.00', 1),
(36, 14, '2021-02-14 22:29:10', 47, '25.00', 1),
(37, 14, '2021-02-14 23:01:05', 5, '0.25', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `nofactura` bigint(11) NOT NULL,
  `fecha` datetime NOT NULL DEFAULT current_timestamp(),
  `usuario` int(11) DEFAULT NULL,
  `codcliente` int(11) DEFAULT NULL,
  `totalfactura` decimal(10,2) DEFAULT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`nofactura`, `fecha`, `usuario`, `codcliente`, `totalfactura`, `estatus`) VALUES
(1, '2021-02-13 01:25:29', 1, 1, '1012.21', 1),
(2, '2021-02-13 01:33:35', 1, 7, '403.40', 2),
(3, '2021-02-14 01:09:17', 1, 5, '36.67', 1),
(4, '2021-02-14 01:13:23', 6, 1, '32.62', 1),
(5, '2021-02-14 13:37:13', 1, 1, '0.45', 2),
(6, '2021-02-14 17:26:58', 1, 6, '247.29', 1),
(7, '2021-02-14 17:34:56', 1, 8, '4130.88', 1),
(8, '2021-02-14 17:37:20', 1, 5, '5.25', 2),
(9, '2021-02-14 17:39:19', 1, 6, '0.45', 2),
(10, '2021-02-14 17:49:47', 1, 1, '20.17', 1),
(11, '2021-02-14 20:39:25', 1, 6, '106.10', 2),
(12, '2021-02-14 22:25:03', 1, 8, '6.45', 2),
(13, '2021-02-14 22:59:10', 1, 8, '6.00', 2),
(14, '2021-02-14 23:00:17', 1, 1, '5.00', 2),
(15, '2021-02-14 23:57:12', 1, 1, '1.50', 2),
(16, '2021-02-15 15:21:46', 1, 1, '203.20', 2),
(17, '2021-02-16 13:00:42', 1, 7, '3485.00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `codproducto` int(11) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `proveedor` int(11) DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `existencia` int(11) DEFAULT NULL,
  `date_add` datetime NOT NULL DEFAULT current_timestamp(),
  `usuario_id` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1,
  `foto` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`codproducto`, `descripcion`, `proveedor`, `precio`, `existencia`, `date_add`, `usuario_id`, `estatus`, `foto`) VALUES
(3, 'Miniuns', 11, '20.17', 12, '2021-01-20 22:12:16', 1, 1, 'img_ae4f3d451b690bfcdfe0518ec930caf2.jpg'),
(5, 'cafesito  ', 13, '21.16', 79, '2021-01-21 15:03:38', 1, 1, 'img_2a7ae776bd256159877e15ab406445d3.jpg'),
(6, 'pavilon', 11, '348.50', 10, '2021-01-22 11:56:31', 1, 1, 'img_da0a144a0dfdafa029db670e2ef784a5.jpg'),
(11, 'ss', 7, '627.60', 10, '2021-01-24 19:11:30', 1, 0, 'img_producto.png'),
(12, 'Teclado usb', 7, '250.00', 182, '2021-02-01 18:20:46', 1, 1, 'img_producto.png'),
(13, 'oreo', 1, '0.45', 8, '2021-02-01 22:39:18', 1, 1, 'img_producto.png'),
(14, 'oreo ', 1, '0.44', 21, '2021-02-01 23:23:36', 1, 1, 'img_d79a71c8d76aed4cba74b1f19116ee83.jpg'),
(15, 'tomate', 7, '0.30', 41, '2021-02-02 12:18:40', 1, 1, 'img_producto.png'),
(16, 'lechuga ', 7, '0.25', 48, '2021-02-07 08:53:53', 1, 1, 'img_4d5a29806fd7567156d36c6f9f949fe8.jpg'),
(17, 'JAbon azul ', 7, '0.75', 8, '2021-02-08 11:37:44', 1, 1, 'img_0af0eec58d8ab78726b7bf58743af8c9.jpg');

--
-- Disparadores `producto`
--
DELIMITER $$
CREATE TRIGGER `entradas_A_I` AFTER INSERT ON `producto` FOR EACH ROW BEGIN
            INSERT INTO entradas (codproducto,cantidad ,precio,usuario_id)
            VALUES(new.codproducto,new.existencia,new.precio,new.usuario_id);
        END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `codproveedor` int(11) NOT NULL,
  `proveedor` varchar(100) DEFAULT NULL,
  `contacto` varchar(100) DEFAULT NULL,
  `telefono` text DEFAULT NULL,
  `direccion` text DEFAULT NULL,
  `date_add` datetime NOT NULL DEFAULT current_timestamp(),
  `usuario_id` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`codproveedor`, `proveedor`, `contacto`, `telefono`, `direccion`, `date_add`, `usuario_id`, `estatus`) VALUES
(1, 'BICA', 'Claudia Rosales', '789877889', 'Avenida las Americas', '2021-01-19 12:04:42', 1, 1),
(2, 'CASIO', 'Jorge Herrera', '565656565656', 'Calzada Las Flores', '2021-01-19 12:04:42', 1, 0),
(3, 'Omega', 'Julio Estrada', '982877489', 'Avenida Elena Zona 4, Guatemala', '2021-01-19 12:04:42', 1, 1),
(4, 'Dell Compani', 'Roberto Estrada', '2147483647', 'Guatemala, Guatemala', '2021-01-19 12:04:42', 1, 1),
(5, 'Olimpia S.A', 'Elena Franco Morales', '564535676', '5ta. Avenida Zona 4 Ciudad', '2021-01-19 12:04:42', 1, 1),
(6, 'Oster', 'Fernando Guerra', '78987678', 'Calzada La Paz, Guatemala', '2021-01-19 12:04:42', 1, 1),
(7, 'ACELTECSA S.A', 'Ruben PÃ©rez', '789879889', 'Colonia las Victorias', '2021-01-19 12:04:42', 1, 1),
(8, 'Sony', 'Julieta Contreras', '89476787', 'Antigua Guatemala', '2021-01-19 12:04:42', 1, 1),
(9, 'VAIO', 'Felix Arnoldo Rojas', '476378276', 'Avenida las Americas Zona 13', '2021-01-19 12:04:42', 1, 1),
(10, 'SUMAR', 'Oscar Maldonado', '788376787', 'Colonia San Jose, Zona 5 Guatemala', '2021-01-19 12:04:42', 1, 1),
(11, 'HP', 'Angel Cardona', '2147483647', '5ta. calle zona 4 Guatemala', '2021-01-19 12:04:42', 1, 1),
(12, 'jp', 'moises', '0981967284', 'los bajos', '2021-01-19 12:24:31', 1, 1),
(13, 'Ales', 'Luis', '0968646', 'manta', '2021-01-20 22:19:25', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `idrol` int(11) NOT NULL,
  `rol` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idrol`, `rol`) VALUES
(1, 'Administrador'),
(2, 'Supervisor'),
(3, 'Vendedor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `usuario` varchar(15) DEFAULT NULL,
  `clave` varchar(100) DEFAULT NULL,
  `rol` int(11) DEFAULT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre`, `correo`, `usuario`, `clave`, `rol`, `estatus`) VALUES
(1, 'Moises', 'admin@gmail.com', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1, 1),
(2, 'josue', 'josue20@gmail.com', '  matias0', '827ccb0eea8a706c4c34a16891f84e7b', 1, 1),
(3, 'matias herreras', 'mhdxdfc@gmail.com', 'admin12', 'e00cf25ad42683b3df678c61f42c6bda', 1, 1),
(4, 'josue', 'mh@gmail.com', 'mh', 'ed8f5b7e74398143b43a93dc753618ae', 1, 1),
(5, 'moi', 'mh1@gmail.com', 'mh1', 'd2cd4d48dcb122a600b5be3e1a78412d', 2, 0),
(6, 'luis', 'luis@gmail.com', 'luis', '827ccb0eea8a706c4c34a16891f84e7b', 3, 1),
(7, 'fgttr', 'matias1@gmail.com', 'matias', '090c36e3bb39377468363197afb3e91b', 1, 1),
(8, 'moi', 'moi@gmail.com', 'moi', '8f8ad28dd6debff410e630ae13436709', 1, 1),
(9, 'lenin', 'lenin@gmail.com', 'lenin', '827ccb0eea8a706c4c34a16891f84e7b', 1, 1),
(10, 'chino', 'chino@gmail.com', 'chino', '242c3d4de7eb843333cb8653348637bd', 2, 1),
(11, 'alvia', 'alvia@gmail.com', 'alvia', '827ccb0eea8a706c4c34a16891f84e7b', 2, 1),
(12, 'tovi', 'tovi@gmail.com', 'tovi', '827ccb0eea8a706c4c34a16891f84e7b', 3, 1),
(13, 'javi', 'javi@gmail.com', 'javi', 'a14f8a540e78dae706d255750010a0f8', 3, 1),
(14, 'kel', 'kel@gamil.com', 'kel', 'd37aebc5ce74fd9e4e744bdb9a9ba06b', 1, 1),
(15, 'liz', 'liz@gmail.com', 'liz', '56d50a506cbbfb4a918a26898a51c1aa', 2, 1),
(16, 'rosa', 'rosa@gmail.com', 'rosa', '84109ae4b4178430b8174b1dfef9162b', 3, 1),
(17, 'morales', 'morales@gmail.com', 'morales', '827ccb0eea8a706c4c34a16891f84e7b', 2, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detallefactura`
--
ALTER TABLE `detallefactura`
  ADD PRIMARY KEY (`correlativo`),
  ADD KEY `codproducto` (`codproducto`),
  ADD KEY `nofactura` (`nofactura`);

--
-- Indices de la tabla `detalle_temp`
--
ALTER TABLE `detalle_temp`
  ADD PRIMARY KEY (`correlativo`),
  ADD KEY `nofactura` (`token_user`),
  ADD KEY `codproducto` (`codproducto`);

--
-- Indices de la tabla `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`correlativo`),
  ADD KEY `codproducto` (`codproducto`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`nofactura`),
  ADD KEY `usuario` (`usuario`),
  ADD KEY `codcliente` (`codcliente`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`codproducto`),
  ADD KEY `proveedor` (`proveedor`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`codproveedor`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idrol`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `rol` (`rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `detallefactura`
--
ALTER TABLE `detallefactura`
  MODIFY `correlativo` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `detalle_temp`
--
ALTER TABLE `detalle_temp`
  MODIFY `correlativo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `entradas`
--
ALTER TABLE `entradas`
  MODIFY `correlativo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `nofactura` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `codproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `codproveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `idrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`idusuario`);

--
-- Filtros para la tabla `detallefactura`
--
ALTER TABLE `detallefactura`
  ADD CONSTRAINT `detallefactura_ibfk_1` FOREIGN KEY (`nofactura`) REFERENCES `factura` (`nofactura`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detallefactura_ibfk_2` FOREIGN KEY (`codproducto`) REFERENCES `producto` (`codproducto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `detalle_temp`
--
ALTER TABLE `detalle_temp`
  ADD CONSTRAINT `detalle_temp_ibfk_2` FOREIGN KEY (`codproducto`) REFERENCES `producto` (`codproducto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `entradas`
--
ALTER TABLE `entradas`
  ADD CONSTRAINT `entradas_ibfk_1` FOREIGN KEY (`codproducto`) REFERENCES `producto` (`codproducto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `factura_ibfk_2` FOREIGN KEY (`codcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`proveedor`) REFERENCES `proveedor` (`codproveedor`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `producto_ibfk_2` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`rol`) REFERENCES `rol` (`idrol`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
